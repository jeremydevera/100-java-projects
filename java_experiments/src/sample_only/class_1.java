package sample_only;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;

public class class_1 {

	private JFrame frmFibonacci;
	public static int numberOfItem = 0;
	public static JTextField txt_num = new JTextField();
	public static JLabel lblError = new JLabel("");
	public static JTextArea textArea = new JTextArea();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					class_1 window = new class_1();
					window.frmFibonacci.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	static String items = null;
	public static String getNum(int num) {
		StringBuilder sb = new StringBuilder();
		
		int num1 = 0;
		int num2 = 1;
		int sumOfPrevious = 0;
		
		for(int i = 1; i <= num; i++) {
			items = num2 + ", ";
			sb.append(items);
			sumOfPrevious = num1 + num2;
			num1 = num2;
			num2 = sumOfPrevious;
		}
		System.out.print(sb);
		return sb.toString();
	}

	/**
	 * Create the application.
	 */
	public class_1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		
		frmFibonacci = new JFrame();
		frmFibonacci.setTitle("Fibonacci");
		frmFibonacci.setResizable(false);
		frmFibonacci.getContentPane().setBackground(Color.BLACK);
		frmFibonacci.getContentPane().setLayout(null);

		
		JLabel Author = new JLabel("Author: Jeremy Adrian De Vera");
		Author.setForeground(new Color(0, 255, 0));
		Author.setFont(new Font("Consolas", Font.PLAIN, 15));
		Author.setBounds(10, 10, 237, 14);
		frmFibonacci.getContentPane().add(Author);
		
		JLabel Title = new JLabel("Title: Fibonacci");
		Title.setForeground(Color.GREEN);
		Title.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title.setBounds(10, 30, 128, 14);
		frmFibonacci.getContentPane().add(Title);
		
		JLabel lblDate = new JLabel("Date: Apr/23/2020");
		lblDate.setForeground(Color.GREEN);
		lblDate.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblDate.setBounds(319, 10, 136, 14);
		frmFibonacci.getContentPane().add(lblDate);
		
		JLabel Title_1 = new JLabel("=======================================================");
		Title_1.setForeground(Color.GREEN);
		Title_1.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title_1.setBounds(10, 49, 445, 14);
		frmFibonacci.getContentPane().add(Title_1);
		
		JLabel Title_2 = new JLabel("Definition:");
		Title_2.setForeground(Color.GREEN);
		Title_2.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title_2.setBounds(10, 75, 105, 14);
		frmFibonacci.getContentPane().add(Title_2);
		
		JTextArea txtrInFibonacciSeries = new JTextArea();
		txtrInFibonacciSeries.setWrapStyleWord(true);
		txtrInFibonacciSeries.setLineWrap(true);
		txtrInFibonacciSeries.setText("In fibonacci series, next number is the sum of previous two numbers for example 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 etc.");
		txtrInFibonacciSeries.setBackground(new Color(0, 0, 0));
		txtrInFibonacciSeries.setForeground(new Color(255, 0, 255));
		txtrInFibonacciSeries.setFont(new Font("Consolas", Font.PLAIN, 15));
		txtrInFibonacciSeries.setBounds(10, 100, 445, 62);
		frmFibonacci.getContentPane().add(txtrInFibonacciSeries);
		
		JLabel Title_1_1 = new JLabel("=======================================================");
		Title_1_1.setForeground(Color.GREEN);
		Title_1_1.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title_1_1.setBounds(10, 174, 445, 14);
		frmFibonacci.getContentPane().add(Title_1_1);
		
		JLabel Title_2_1 = new JLabel("Application:");
		Title_2_1.setForeground(Color.GREEN);
		Title_2_1.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title_2_1.setBounds(10, 199, 105, 14);
		frmFibonacci.getContentPane().add(Title_2_1);
		
		JLabel num_elements = new JLabel("Enter number of elements:");
		num_elements.setForeground(new Color(255, 255, 0));
		num_elements.setFont(new Font("Consolas", Font.PLAIN, 15));
		num_elements.setBounds(10, 227, 208, 14);
		frmFibonacci.getContentPane().add(num_elements);
		

		txt_num.setToolTipText("enter number only");
		txt_num.setHorizontalAlignment(SwingConstants.CENTER);
		txt_num.setForeground(new Color(255, 255, 0));
		txt_num.setFont(new Font("Consolas", Font.PLAIN, 15));
		txt_num.setBackground(new Color(0, 0, 0));
		txt_num.setBounds(219, 224, 109, 20);
		frmFibonacci.getContentPane().add(txt_num);
		txt_num.setColumns(10);
		
		txt_num.addKeyListener(new KeyAdapter() {
	         public void keyPressed(KeyEvent ke) {
	            if (ke.getKeyChar() >= '0' && ke.getKeyChar() <= '9' || ke.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
	            	txt_num.setEditable(true);
	            	lblError.setText("");
	            } 
	            else if(ke.getKeyCode() == KeyEvent.VK_ENTER) {
	            	onbuttonpress();
	            } else {
	            	txt_num.setEditable(false);
	            	lblError.setText("* Enter only numeric digits(0-9)");
	            }
	         }
	      });
		
		
		JButton btnNewButton = new JButton("Enter!");
		btnNewButton.setForeground(new Color(0, 255, 0));
		btnNewButton.setFont(new Font("Consolas", Font.PLAIN, 15));
		btnNewButton.setBackground(new Color(0, 0, 0));
		btnNewButton.setBounds(346, 224, 109, 20);
		frmFibonacci.getContentPane().add(btnNewButton);
		
		JLabel Title_1_1_1 = new JLabel("=======================================================");
		Title_1_1_1.setForeground(Color.GREEN);
		Title_1_1_1.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title_1_1_1.setBounds(10, 267, 445, 14);
		frmFibonacci.getContentPane().add(Title_1_1_1);
		
		JLabel Title_2_1_1 = new JLabel("Output:");
		Title_2_1_1.setForeground(Color.GREEN);
		Title_2_1_1.setFont(new Font("Consolas", Font.PLAIN, 15));
		Title_2_1_1.setBounds(10, 292, 67, 14);
		frmFibonacci.getContentPane().add(Title_2_1_1);
		
		
		lblError.setForeground(Color.RED);
		lblError.setFont(new Font("Consolas", Font.PLAIN, 15));
		lblError.setBounds(78, 292, 377, 14);
		frmFibonacci.getContentPane().add(lblError);
		

		textArea.setForeground(new Color(0, 255, 255));
		textArea.setBackground(Color.BLACK);
		textArea.setFont(new Font("Consolas", Font.PLAIN, 15));
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		textArea.setBounds(10, 317, 445, 100);
		frmFibonacci.getContentPane().add(textArea);
		frmFibonacci.setBounds(100, 100, 481, 457);
		frmFibonacci.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

		btnNewButton.addActionListener(new ActionListener() 	{
			public void actionPerformed(ActionEvent e) {
				onbuttonpress();
			}
		});	
	}
	
	public static void onbuttonpress() {
		if(txt_num.getText().trim().isEmpty()) {
			lblError.setText("* Input is null");
		} else {
			String get_txtNone = txt_num.getText();
			int num = Integer.parseInt(get_txtNone);
			
			if(num < 0) {
				lblError.setText("* Please input a positive number");
			} else if(num == 0) {
				lblError.setText("* Please specify number of items");
			} else if(num > 40) {
				lblError.setText("* Please input number between 1-40");
			} else {	
				lblError.setText("");
				String finalResult = getNum(num);
				textArea.setText(finalResult);
				
			}
		}
	}
}
