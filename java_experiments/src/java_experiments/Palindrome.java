package java_experiments;
import java.util.Scanner;
public class Palindrome {
    public static void main (String [] args){ 
        Scanner in = new Scanner(System.in);
        String word = in.next();
        String result = null;

        for(int i = 0; i < word.length() / 2; i++){
            if(word.charAt(i) != word.charAt(word.length() - 1 - i)){
                result = "not palindrome";
                break;
            } else {
                result = "palindrome";
            }

        }
        System.out.print(result);
        //System.out.print(isPalindrome(word));
    }

    public static boolean isPalindrome(String word){
        boolean result = false;

        for(int i = 0; i < word.length() / 2; i++){
            if(word.charAt(i) != word.charAt(word.length() - 1 - i)){
                result = false;
                break;
            } else {
                result = true;
            }
        }
        return result;
    }
}