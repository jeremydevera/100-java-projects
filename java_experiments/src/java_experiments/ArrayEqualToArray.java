package java_experiments;

import java.util.*;
public class ArrayEqualToArray {

    public static void main (String [] a) {
        String [] x = {"a", "b", "c"};
        String [] y = {"a", "b", "d"};

        System.out.print(equal(x,y));
    }

    public static boolean equal(String [] x, String [] y){
        boolean result = false;
        if(Arrays.toString(x).equals(Arrays.toString(y))){
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}