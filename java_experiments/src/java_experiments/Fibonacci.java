package java_experiments;

public class Fibonacci {
    public static void main (String [] args)
    {
        int num1 = 1, num2 = 3, size = 5, sumOfPreviousTwo = 0;

        for(int i = 0; i < size; i++)
        {
            sumOfPreviousTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPreviousTwo;

            System.out.print(num1 + " ");
        }
    }
}