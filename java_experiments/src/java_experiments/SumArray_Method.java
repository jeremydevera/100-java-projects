package java_experiments;

public class SumArray_Method {
    public static void main (String [] a)
    {
        System.out.println(sum(new int[]{10,20,30,40,50}));
    }

    public static int sum(int [] x)
    {
        int total = 0;

        for(int x1 : x)
        {
            total = total + x1;
        }

        return total;
    }
}