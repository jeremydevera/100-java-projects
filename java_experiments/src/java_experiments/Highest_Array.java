package java_experiments;

public class Highest_Array {
    public static void main (String [] args)
    {
        System.out.println(average(new int[] {1,1,2,1}));
    }

    public static double average(int [] x){
        double total = 0.0;

        for(int items: x){
            total = items + total;
        }
        return total / x.length;
    }
}