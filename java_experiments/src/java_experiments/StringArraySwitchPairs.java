package java_experiments;

public class StringArraySwitchPairs {

    public static void main (String [] args){
        String [] pairs = {"a", "b", "c"};
        swapPairs(pairs);
    }

    public static void swapPairs(String[] array) {

        for (int i = 0; i < array.length / 2; i++) {
            String temp = array[2 * i];
            array[2 * i] = array[2 * i + 1];
            array[2 * i + 1] = temp;
        }
    }
}