package java_experiments;

import java.util.*;
public class ArrayList_App{
	
    static ArrayList <String> list = new ArrayList <String> ();
    static Scanner in = new Scanner (System.in);
    int input = 0;
    
    public static void main (String [] args){
    	boolean exit = true;
    	int position = 0;
    	String item;
    	
    	while (exit != false) {
    		choices();
    		System.out.print("Choice: ");
	        int input = in.nextInt();

	        switch(input){
	            case 1: 
	            		System.out.print("Input String: ");
	            		in.nextLine();
	            		item = in.nextLine();
	            		addTopArray(item);
	            		choices();
	            		
	            	break;
	            	
	            case 2:       				
    					System.out.print("At Position : ");
        				position = in.nextInt();
        				position = position - 1;
        				
        				if(position > list.size()) {
        					System.out.println("Size of list is only " + list.size() + " try again");
        					break;
        				}
        				
        				System.out.print("Input String: ");
			            in.nextLine();
		        		item = in.nextLine();
		        		addAtPosition(item,position);
				        		
	            	break;
	            	
	            	
	            case 3:
	            		System.out.print("Input item to remove: ");
	            		System.out.println();
	            		in.nextLine();
	            		item = in.nextLine();
	            		removeUsingItem(item);
	            		
	            	break;
	            	
	            case 4:
	            		System.out.print("What position to delete: ");
	            		position = in.nextInt();
	            		removeUsingIndex(position);
	            	break;
	            		
		        		
	            	
	            case 10: System.out.println("==================================");
	            		 System.out.println("Elements are: ");
	            		 for(int i = 0; i < list.size(); i++) {
	            			 System.out.println((i + 1) + ". " + list.get(i));
	            		 }
	            		 System.out.println();
	            		 System.out.println("==================================");
	            	break;

	            default: System.out.println("Something Happened");
	            	break;
	        }
    	}
    }

    static void choices(){
    	System.out.println();
        System.out.println(" 1. Add Element");
        System.out.println(" 2. Add at Position");
        System.out.println(" 3. Remove Using [Name]");
        System.out.println(" 4. Remove at Position");
        System.out.println(" 5. Set at Position");
        System.out.println(" 6. Get Index Using Search");
        System.out.println(" 7. Search Using Index");
        System.out.println(" 8. Get Size of Array");
        System.out.println(" 9. Does Array Contains [Name]");
        System.out.println("10. Display Array");
        System.out.println("11. Clear Array");
        System.out.println("==================================");
    }

    static void addTopArray(String item){
        list.add(item);
    }
    
    static void addAtPosition(String item, int position) {
    	list.add(position, item);
    }
    
    static void removeUsingItem(String item) {
    	list.remove(item);
    }
    
    static void removeUsingIndex(int item) {
    	list.remove(item);
    }
}