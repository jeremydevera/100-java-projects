package java_experiments;

public class ArrayLessThanOtherArray {
    public static void main (String [] a){
        int [] x = {67, 11, 98, -4};
        int [] y = {50, 41, 600, 1};

        System.out.print(allLess(x,y));
    }

    public static boolean allLess(int [] x, int [] y){
        boolean result = false;

        if(x.length == y.length){
            for(int i = 0; i < x.length; i++){
                if(x[i] < y[i]){
                    result = true;
                } else {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }

        return result;
    }
}