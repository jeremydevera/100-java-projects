package java_experiments;

public class GCF {
    public static void main (String [] args){
        System.out.println(findGCD(81,63));
    }

    public static int findGCD(int number1, int number2){
        if(number2 == 0){
            return number1;
        }
        return findGCD(number2, number1%number2);
    }
}