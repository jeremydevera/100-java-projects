package java_experiments;

public class StringArrayOccurence {
    public static void main(String [] args) {
        String [] arr = {"2","3"};

        System.out.println(countStrings(arr, "2"));
        
    }
    
    public static int countStrings(String[] string, String s) {
        int i = 0;
        for(String x : string) {
            if(x.equals(s)) {
                i++;
            }
        }
        return i;
    }
}