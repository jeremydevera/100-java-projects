package java_experiments;

public class ReverseArray {
    public static void main (String [] args){
        int [] arr = {22,23,34,45};

        for(int i = 0 ; i < arr.length; i++){
            System.out.print(arr[arr.length - i - 1] + " ");
        }
    }
}